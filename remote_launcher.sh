#!/bin/sh
set -eu
scp 4bit.x launcher.sh "$1":/tmp/
ssh "$1" "cd /tmp/; ./launcher.sh; rm launcher.sh 4bit.x"
